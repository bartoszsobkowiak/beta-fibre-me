<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#1273C1</headerColor>
        <logo>enxB2B__enxooCommerceLogo4</logo>
        <logoVersion>1</logoVersion>
    </brand>
    <description>Enxoo for Communications: Order Management Application</description>
    <formFactors>Large</formFactors>
    <label>Enxoo for Comms OM</label>
    <navType>Console</navType>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxCPQ__CPQ_Charge_Tier_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Product2</pageOrSobjectType>
        <recordType>Product2.enxCPQ__Charge_Tier</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxCPQ__CPQ_Charge_Tier_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Product2</pageOrSobjectType>
        <recordType>Product2.enxCPQ__Charge_Tier</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxCPQ__CPQ_Option_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Product2</pageOrSobjectType>
        <recordType>Product2.enxCPQ__Option</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxCPQ__CPQ_Option_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Product2</pageOrSobjectType>
        <recordType>Product2.enxCPQ__Option</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Opportunity_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.enxB2B__Opportunity</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Opportunity_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.enxB2B__Opportunity</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Opportunity_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.enxB2B__Opportunity_Locked</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Opportunity_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.enxB2B__Opportunity_Locked</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_POP_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxB2B__POP__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_POP_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxB2B__POP__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Opportunity_Product_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>OpportunityLineItem</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Opportunity_Product_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>OpportunityLineItem</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Cost_Item_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxB2B__CostItem__c</pageOrSobjectType>
        <recordType>enxB2B__CostItem__c.enxB2B__Resource_Locked</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Cost_Item_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxB2B__CostItem__c</pageOrSobjectType>
        <recordType>enxB2B__CostItem__c.enxB2B__Cost_Item</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Cost_Item_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxB2B__CostItem__c</pageOrSobjectType>
        <recordType>enxB2B__CostItem__c.enxB2B__Cost_Item</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Cost_Item_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxB2B__CostItem__c</pageOrSobjectType>
        <recordType>enxB2B__CostItem__c.enxB2B__Cost_Item_Locked</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Cost_Item_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxB2B__CostItem__c</pageOrSobjectType>
        <recordType>enxB2B__CostItem__c.enxB2B__Cost_Item_Locked</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Cost_Item_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxB2B__CostItem__c</pageOrSobjectType>
        <recordType>enxB2B__CostItem__c.enxB2B__Resource</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Cost_Item_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxB2B__CostItem__c</pageOrSobjectType>
        <recordType>enxB2B__CostItem__c.enxB2B__Resource</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Cost_Item_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxB2B__CostItem__c</pageOrSobjectType>
        <recordType>enxB2B__CostItem__c.enxB2B__Resource_Locked</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Account_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Account_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Location_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxCPQ__Location__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Provisioning_Plan_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxB2B__ProvisioningPlan__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Provisioning_Plan_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxB2B__ProvisioningPlan__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Location_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxCPQ__Location__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Category_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxCPQ__Category__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Category_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxCPQ__Category__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxCPQ__CPQ_Attribute_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxCPQ__Attribute__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxCPQ__CPQ_Attribute_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxCPQ__Attribute__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Quote_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Quote</pageOrSobjectType>
        <recordType>Quote.enxB2B__Quote</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Quote_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Quote</pageOrSobjectType>
        <recordType>Quote.enxB2B__Quote</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Quote_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Quote</pageOrSobjectType>
        <recordType>Quote.enxB2B__Quote_Locked</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Quote_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Quote</pageOrSobjectType>
        <recordType>Quote.enxB2B__Quote_Locked</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Quote_Line_Item_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>QuoteLineItem</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Quote_Line_Item_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>QuoteLineItem</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Cart_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxCPQ__Cart__c</pageOrSobjectType>
        <recordType>enxCPQ__Cart__c.enxCPQ__Cart</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Cart_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxCPQ__Cart__c</pageOrSobjectType>
        <recordType>enxCPQ__Cart__c.enxCPQ__Cart</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxCPQ__CPQ_Cart_Item_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxCPQ__Cart__c</pageOrSobjectType>
        <recordType>enxCPQ__Cart__c.enxCPQ__Cart_Item</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxCPQ__CPQ_Cart_Item_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxCPQ__Cart__c</pageOrSobjectType>
        <recordType>enxCPQ__Cart__c.enxCPQ__Cart_Item</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Price_Rule_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxCPQ__PriceRule__c</pageOrSobjectType>
        <recordType>enxCPQ__PriceRule__c.enxCPQ__Standard</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Price_Rule_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxCPQ__PriceRule__c</pageOrSobjectType>
        <recordType>enxCPQ__PriceRule__c.enxCPQ__Standard</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Price_Rule_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxCPQ__PriceRule__c</pageOrSobjectType>
        <recordType>enxCPQ__PriceRule__c.enxCPQ__Tiered</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Price_Rule_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxCPQ__PriceRule__c</pageOrSobjectType>
        <recordType>enxCPQ__PriceRule__c.enxCPQ__Tiered</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Service_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxB2B__Service__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Service_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxB2B__Service__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Product2_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Product2</pageOrSobjectType>
        <recordType>Product2.enxCPQ__Product</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Product2_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Product2</pageOrSobjectType>
        <recordType>Product2.enxCPQ__Product</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Order_Product_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>OrderItem</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Order_Product_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>OrderItem</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxCPQ__CPQ_Charge_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Product2</pageOrSobjectType>
        <recordType>Product2.enxCPQ__Charge</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxCPQ__CPQ_Charge_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Product2</pageOrSobjectType>
        <recordType>Product2.enxCPQ__Charge</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Bundle_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Product2</pageOrSobjectType>
        <recordType>Product2.enxCPQ__Bundle</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Bundle_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Product2</pageOrSobjectType>
        <recordType>Product2.enxCPQ__Bundle</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Case_layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Case</pageOrSobjectType>
        <recordType>Case.enxB2B__Customer_Trouble_Ticket</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Case_layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Case</pageOrSobjectType>
        <recordType>Case.enxB2B__Customer_Trouble_Ticket</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Case_layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Case</pageOrSobjectType>
        <recordType>Case.enxB2B__General_Inquiry</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Case_layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Case</pageOrSobjectType>
        <recordType>Case.enxB2B__General_Inquiry</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Case_Affected_Service_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Case</pageOrSobjectType>
        <recordType>Case.enxB2B__Affected_Service</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Case_Affected_Service_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Case</pageOrSobjectType>
        <recordType>Case.enxB2B__Affected_Service</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Attribute_Set_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxCPQ__AttributeSet__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Attribute_Set_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxCPQ__AttributeSet__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Case_layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Case</pageOrSobjectType>
        <recordType>Case.enxB2B__Network_Trouble_Ticket</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Case_layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Case</pageOrSobjectType>
        <recordType>Case.enxB2B__Planned_Maintenance</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Case_layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Case</pageOrSobjectType>
        <recordType>Case.enxB2B__Planned_Maintenance</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Case_layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Case</pageOrSobjectType>
        <recordType>Case.enxB2B__Network_Trouble_Ticket</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Resource_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Product2</pageOrSobjectType>
        <recordType>Product2.enxB2B__Resource</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Resource_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Product2</pageOrSobjectType>
        <recordType>Product2.enxB2B__Resource</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Provisioning_Task_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxB2B__ProvisioningTask__c</pageOrSobjectType>
        <recordType>enxB2B__ProvisioningTask__c.enxB2B__Automatic</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Provisioning_Task_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxB2B__ProvisioningTask__c</pageOrSobjectType>
        <recordType>enxB2B__ProvisioningTask__c.enxB2B__Automatic</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Provisioning_Task_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxB2B__ProvisioningTask__c</pageOrSobjectType>
        <recordType>enxB2B__ProvisioningTask__c.enxB2B__Manual</recordType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Provisioning_Task_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>enxB2B__ProvisioningTask__c</pageOrSobjectType>
        <recordType>enxB2B__ProvisioningTask__c.enxB2B__Manual</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Order_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Order</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Standard</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>enxB2B__B2B_Order_Layout</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Order</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <tab>standard-Account</tab>
    <tab>standard-Opportunity</tab>
    <tab>standard-Order</tab>
    <tab>standard-WorkOrder</tab>
    <tab>enxB2B__Service__c</tab>
    <uiType>Lightning</uiType>
    <utilityBar>enxB2B__Enxoo_for_Comms_OM_UtilityBar</utilityBar>
    <workspaceMappings>
        <mapping>
            <tab>enxB2B__Service__c</tab>
        </mapping>
        <mapping>
            <tab>standard-Account</tab>
        </mapping>
        <mapping>
            <tab>standard-Opportunity</tab>
        </mapping>
        <mapping>
            <tab>standard-Order</tab>
        </mapping>
        <mapping>
            <fieldName>enxB2B__Order_Item__c</fieldName>
            <tab>standard-WorkOrder</tab>
        </mapping>
    </workspaceMappings>
</CustomApplication>

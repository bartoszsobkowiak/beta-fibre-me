import sys
import argparse
from json import dumps
from httplib2 import Http


IMAGE_ICON      = "https://i.postimg.cc/2jcTwSTp/pengsad-1.jpg"
PIPELINES_URL   = "https://bitbucket.org/bartoszsobkowiak/beta-fibre-me/addon/pipelines/home#!/results/"
WEBHOOK         = "AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI"


def createMessage(args):
    args_buildnumber = {
        "keyValue": {
        "topLabel": "Build number",
        "content": args.buildnumber
        }
    }            
    args_branch = {
        "keyValue": {
        "topLabel": "Branch",
        "content": args.branch
        }
    }
    args_pullrequest = {
        "keyValue": {
        "topLabel": "Pull Request ID",
        "content": args.pullrequest
        }
    }
    args_url = f"{PIPELINES_URL}{args.buildnumber}"

    JSON_TEMPLATE = {
        "cards": [
            {
            "header": {
                "title": "Pizza Bot Customer Support",
                "subtitle": 'FAILED',
                "imageUrl": IMAGE_ICON
            },
            "sections": [
                {
                "widgets": [
                    args_buildnumber,
                    args_branch
                ]
                },
                {
                "widgets": [
                    {
                        "buttons": [
                            {
                            "textButton": {
                                "text": "OPEN PIPELINE",
                                "onClick": {
                                    "openLink": {
                                        "url": args_url
                                    }
                                }
                            }
                            }
                        ]
                    }
                ]
                }
            ]
            }
        ]
        }
    # JSON_TEMPLATE = {
    # "cards": [
    #     {
    #     "header": {
    #         "title": "Pizza Bot Customer Support",
    #         "subtitle": PIPELINES_URL,
    #         "imageUrl": "https://goo.gl/aeDtrS"
    #     },
    #     "sections": [
    #         {
    #         "widgets": [
    #             {
    #                 "keyValue": {
    #                 "topLabel": "Order No.",
    #                 "content": "args.pullrequest"
    #                 }
    #             },
    #             {
    #                 "keyValue": {
    #                 "topLabel": "Status",
    #                 "content": "In Delivery"
    #                 }
    #             }
    #         ]
    #         },
    #         {
    #         "header": "Location",
    #         "widgets": [
    #             {
    #             "image": {
    #                 "imageUrl": "https://maps.googleapis.com/..."
    #             }
    #             }
    #         ]
    #         },
    #         {
    #         "widgets": [
    #             {
    #                 "buttons": [
    #                     {
    #                     "textButton": {
    #                         "text": "OPEN ORDER",
    #                         "onClick": {
    #                         "openLink": {
    #                             "url": "https://example.com/orders/..."
    #                         }
    #                         }
    #                     }
    #                     }
    #                 ]
    #             }
    #         ]
    #         }
    #     ]
    #     }
    # ]
    # }
    return JSON_TEMPLATE


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-e", "--exitcode",     nargs='?', default='0',             type=str, help="BITBUCKET_EXIT_CODE")
    parser.add_argument("-n", "--buildnumber",  nargs='?', default='not specified', type=str, help="BITBUCKET_BUILD_NUMBER")
    parser.add_argument("-b", "--branch",       nargs='?', default='not specified', type=str, help="BITBUCKET_BRANCH")
    parser.add_argument("-p", "--pullrequest",  nargs='?', default='not specified', type=str, help="BITBUCKET_PR_ID")
    parser.add_argument("-s", "--chatspace",    type=str, help="CHAT_SPACE")
    parser.add_argument("-k", "--chatkey",      type=str, help="CHAT_KEY")
    parser.add_argument("-c", "--chattoken",    type=str, help="CHAT_TOKEN")
    parser.add_argument("-u", "--chatuser",     type=str, help="CHAT_USER1")
    parser.add_argument("-v", "--nextchatuser", type=str, help="CHAT_USER2")

    args = parser.parse_args()
    print('DEBUG:', args)

    if args.exitcode != '0' and args.exitcode != '':
        url = f'https://chat.googleapis.com/v1/spaces/{args.chatspace}/messages?key={args.chatkey}&token={args.chattoken}'
        bot_message = createMessage(args)
    
        message_headers = {'Content-Type': 'application/json; charset=UTF-8'}
        http_obj = Http()

        response = http_obj.request(
            uri=url,
            method='POST',
            headers=message_headers,
            body=dumps(bot_message),
        )

        print('DEBUG:', response)

if __name__ == '__main__':
    main()
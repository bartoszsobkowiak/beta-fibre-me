<?xml version="1.0" encoding="UTF-8"?>
<Profile xmlns="http://soap.sforce.com/2006/04/metadata">
    <custom>false</custom>
    <loginIpRanges>
            <description>All</description>
            <endAddress>255.255.255.255</endAddress>
            <startAddress>0.0.0.0</startAddress>
    </loginIpRanges>
    <layoutAssignments>
        <layout>Account-B2B Account Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Contact-B2B Contact Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Contract-enxB2B__B2B Contract Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Opportunity-B2B Opportunity Layout</layout>
        <recordType>Opportunity.Opportunity</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Opportunity-B2B Opportunity Layout Locked</layout>
        <recordType>Opportunity.Opportunity_Locked</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>OpportunityLineItem-B2B Opportunity Product Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Order-B2B Order Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>OrderItem-B2B Order Product Layout 2</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Pricebook2-enxCPQ__CPQ Price Book Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Product2-B2B Resource Layout</layout>
        <recordType>Product2.Resource</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Product2-enxCPQ__CPQ Charge Element Layout</layout>
        <recordType>Product2.enxCPQ__Charge_Element</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Product2-enxCPQ__CPQ Charge Layout</layout>
        <recordType>Product2.enxCPQ__Charge</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Product2-enxCPQ__CPQ Charge Tier Layout</layout>
        <recordType>Product2.enxCPQ__Charge_Tier</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Product2-enxCPQ__CPQ Product Bundle Layout</layout>
        <recordType>Product2.enxCPQ__Bundle</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Product2-enxCPQ__CPQ Product Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Product2-enxCPQ__CPQ Product Layout</layout>
        <recordType>Product2.enxCPQ__Product</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Product2-enxCPQ__CPQ Product Option Layout</layout>
        <recordType>Product2.enxCPQ__Option</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Product2-enxCPQ__CPQ Resource Layout</layout>
        <recordType>Product2.enxCPQ__Resource</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Quote-B2B Quote Layout</layout>
        <recordType>Quote.Quote</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Quote-B2B Quote Layout Locked</layout>
        <recordType>Quote.Quote_Locked</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>QuoteLineItem-B2B Quote Line Item Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkOrder-B2B Work Order Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkOrder-B2B Work Order Layout</layout>
        <recordType>WorkOrder.Automatic</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkOrder-B2B Work Order Layout</layout>
        <recordType>WorkOrder.Manual</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CostItem__c-Cost Item Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CostItem__c-Cost Item Layout</layout>
        <recordType>CostItem__c.Cost_Item</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CostItem__c-Cost Item Layout</layout>
        <recordType>CostItem__c.Resource</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CostItem__c-Cost Item Layout</layout>
        <recordType>CostItem__c.Resource_Locked</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CostItem__c-Cost Item Layout Locked</layout>
        <recordType>CostItem__c.Cost_Item_Locked</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Cost_Item_Bid__c-Cost Item Bid Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>POP__c-POP Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ProvisioningPlanAssignment__c-Provisioning Plan Assignment Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ProvisioningPlan__c-Provisioning Plan Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ProvisioningTaskAssignment__c-Provisioning Task Assignment Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ProvisioningTask__c-B2B Automatic Provisioning Task Layout</layout>
        <recordType>ProvisioningTask__c.Automatic</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ProvisioningTask__c-B2B Manual Provisioning Task Layout</layout>
        <recordType>ProvisioningTask__c.Manual</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Resource__c-Resource Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ServiceAttribute__c-Service Attribute Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Service__c-Service Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__AccountPricebook__c-enxCPQ__Account Pricebook Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__ApprovalRuleCondition__c-enxCPQ__Approval Rule Condition Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__ApprovalRuleSecurity__c-enxCPQ__Approval Rule Security Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__ApprovalRule__c-enxCPQ__Approval Rule Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__AttributeDefaultValue__c-enxCPQ__Attribute Default Value Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__AttributeRule__c-enxCPQ__Attribute Rule Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__AttributeSetAttribute__c-enxCPQ__Attribute Set Attribute Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__AttributeSet__c-enxCPQ__Attribute Set Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__AttributeValueDependency__c-enxCPQ__Attribute Value Dependency Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__AttributeValue__c-enxCPQ__Attribute Value Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__Attribute__c-enxCPQ__Attribute Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__BundleElementOption__c-enxCPQ__Bundle Element Option Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__BundleElementRule__c-enxCPQ__Bundle Element Rule Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__BundleElement__c-enxCPQ__Bundle Element Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__Cart__c-enxCPQ__Cart Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__Cart__c-enxB2B__B2B Cart Layout</layout>
        <recordType>enxCPQ__Cart__c.enxCPQ__Cart</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__Cart__c-enxB2B__B2B Cart Item Layout</layout>
        <recordType>enxCPQ__Cart__c.enxCPQ__Cart_Item</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__Category__c-enxCPQ__Category Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__Image__c-enxCPQ__Image Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__Location__c-B2B Location Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__OrderItemAttribute__c-enxCPQ__Order Item Attribute Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__PriceRuleAction__c-enxCPQ__Price Rule Action Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__PriceRuleCondition__c-enxCPQ__Price Rule Condition Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__PriceRule__c-enxCPQ__Price Rule Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__PriceRule__c-enxCPQ__Discount Rule Layout</layout>
        <recordType>enxCPQ__PriceRule__c.enxCPQ__Discount</recordType>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__ProductAttribute__c-enxCPQ__Product Attribute Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__ProductRelationship__c-enxCPQ__Product Relationship Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>enxCPQ__QuoteLineItemAttribute__c-enxCPQ__Quote Line Item Attribute Layout</layout>
    </layoutAssignments>
    <objectPermissions>
        <allowCreate>true</allowCreate>
        <allowDelete>true</allowDelete>
        <allowEdit>true</allowEdit>
        <allowRead>true</allowRead>
        <modifyAllRecords>false</modifyAllRecords>
        <object>Account</object>
        <viewAllRecords>false</viewAllRecords>
    </objectPermissions>
    <objectPermissions>
        <allowCreate>true</allowCreate>
        <allowDelete>false</allowDelete>
        <allowEdit>true</allowEdit>
        <allowRead>true</allowRead>
        <modifyAllRecords>false</modifyAllRecords>
        <object>Case</object>
        <viewAllRecords>false</viewAllRecords>
    </objectPermissions>
    <objectPermissions>
        <allowCreate>true</allowCreate>
        <allowDelete>true</allowDelete>
        <allowEdit>true</allowEdit>
        <allowRead>true</allowRead>
        <modifyAllRecords>false</modifyAllRecords>
        <object>Contact</object>
        <viewAllRecords>false</viewAllRecords>
    </objectPermissions>
    <objectPermissions>
        <allowCreate>true</allowCreate>
        <allowDelete>true</allowDelete>
        <allowEdit>true</allowEdit>
        <allowRead>true</allowRead>
        <modifyAllRecords>false</modifyAllRecords>
        <object>Opportunity</object>
        <viewAllRecords>false</viewAllRecords>
    </objectPermissions>
    <objectPermissions>
        <allowCreate>true</allowCreate>
        <allowDelete>true</allowDelete>
        <allowEdit>true</allowEdit>
        <allowRead>true</allowRead>
        <modifyAllRecords>false</modifyAllRecords>
        <object>Order</object>
        <viewAllRecords>false</viewAllRecords>
    </objectPermissions>
    <objectPermissions>
        <allowCreate>false</allowCreate>
        <allowDelete>false</allowDelete>
        <allowEdit>false</allowEdit>
        <allowRead>true</allowRead>
        <modifyAllRecords>false</modifyAllRecords>
        <object>Pricebook2</object>
        <viewAllRecords>false</viewAllRecords>
    </objectPermissions>
    <objectPermissions>
        <allowCreate>false</allowCreate>
        <allowDelete>false</allowDelete>
        <allowEdit>false</allowEdit>
        <allowRead>true</allowRead>
        <modifyAllRecords>false</modifyAllRecords>
        <object>Product2</object>
        <viewAllRecords>false</viewAllRecords>
    </objectPermissions>
    <objectPermissions>
        <allowCreate>true</allowCreate>
        <allowDelete>true</allowDelete>
        <allowEdit>true</allowEdit>
        <allowRead>true</allowRead>
        <modifyAllRecords>false</modifyAllRecords>
        <object>Quote</object>
        <viewAllRecords>false</viewAllRecords>
    </objectPermissions>
    <objectPermissions>
        <allowCreate>true</allowCreate>
        <allowDelete>false</allowDelete>
        <allowEdit>true</allowEdit>
        <allowRead>true</allowRead>
        <modifyAllRecords>false</modifyAllRecords>
        <object>WorkOrder</object>
        <viewAllRecords>false</viewAllRecords>
    </objectPermissions>
    <recordTypeVisibilities>
        <default>true</default>
        <recordType>Opportunity.Opportunity</recordType>
        <visible>true</visible>
    </recordTypeVisibilities>
    <recordTypeVisibilities>
        <default>false</default>
        <recordType>Product2.enxCPQ__Bundle</recordType>
        <visible>true</visible>
    </recordTypeVisibilities>
    <recordTypeVisibilities>
        <default>false</default>
        <recordType>Product2.enxCPQ__Charge</recordType>
        <visible>false</visible>
    </recordTypeVisibilities>
    <recordTypeVisibilities>
        <default>false</default>
        <recordType>Product2.enxCPQ__Charge_Element</recordType>
        <visible>false</visible>
    </recordTypeVisibilities>
    <recordTypeVisibilities>
        <default>false</default>
        <recordType>Product2.enxCPQ__Charge_Tier</recordType>
        <visible>false</visible>
    </recordTypeVisibilities>
    <recordTypeVisibilities>
        <default>false</default>
        <recordType>Product2.enxCPQ__Option</recordType>
        <visible>true</visible>
    </recordTypeVisibilities>
    <recordTypeVisibilities>
        <default>false</default>
        <recordType>Product2.enxCPQ__Resource</recordType>
        <visible>true</visible>
    </recordTypeVisibilities>
    <recordTypeVisibilities>
        <default>true</default>
        <recordType>Product2.enxCPQ__Product</recordType>
        <visible>true</visible>
    </recordTypeVisibilities>
    <recordTypeVisibilities>
        <default>true</default>
        <recordType>enxCPQ__Cart__c.enxCPQ__Cart</recordType>
        <visible>true</visible>
    </recordTypeVisibilities>
    <userLicense>Salesforce</userLicense>
</Profile>

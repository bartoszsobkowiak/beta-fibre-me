// 1. CHECKOUT release/x.x.x branch

// 2. BUILD STATIC RESOURCES
------------------------------------------------------------------------------------------
-node v8.9.4 required
-bash required (on windows git bash or linux shell)
-first command - only when new static resource was added or inside dependencies was changedsfdx
------------------------------------------------------------------------------------------
> .\reactInstall.sh
> .\reactBuild.sh

// 3. CONVERT SOURCE TO METADATA FORMAT AND OUTPUT TO “mdapiout” FOLDER
> sfdx force:source:convert --outputdir mdapiout --packagename "Enxoo for Communications"

// 3A. MAKE SURE NEWEST CPQ PACKAGE IS INSTALLED (OPTIONAL)

// 4. DEPLOY SOURCE TO PACKAGING ORG (enxCommsPackagingOrg)
> sfdx force:mdapi:deploy --deploydir mdapiout --targetusername enxB2BPackagingOrg --wait 5

// 5. CREATE MINOR PACKAGE VERSION (enter proper version, sprint and build number)
// 0330Y000000HWJqQAO <- EnxooCPQ core managed package ID
> sfdx force:package1:version:create --packageid 0330O000000Hg3S --name "6.4.0" --description "6.4.0" --version 6.4 --managedreleased --targetusername enxB2BPackagingOrg --wait 10

// 6. LIST AVAILABLE PACKAGE VERSIONS
> sfdx force:package1:version:list -u enxCommsPackagingOrg
METADATAPACKAGEVERSIONID  METADATAPACKAGEID   NAME                                  VERSION  RELEASESTATE  BUILDNUMBER
────────────────────────  ──────────────────  ────────────────────────────────────  ───────  ────────────  ───────────
...  
04t0Y000002NTdAQAW        0330Y000000HWJqQAO  v2 sprint2 build03                    2.203.0  Released      1
04t0Y000002NTeIQAW        0330Y000000HWJqQAO  v2 sprint2 build04                    2.204.0  Released      1

#!/bin/sh
set -e

# CPQ 9.13.0
cpq_package="04t1v000001YbP8";
# B2B 9.7.0
b2b_package="04t3Y000000mfxn";
# Commuunity 0.0.4-1
community_template_id="04t3x000001Gv2WAAS";
# Customer Sales Proces Flow 0.1.0-13
customerflow_id="04t3x000001GuooAAC";
# Enxoo Documents 2.6
docgen_id="04t0X000000SZsp";
# Enxoo Billing 2.46
billing_id="04t3x000001GvW7AAK";
# Billing Connector 0.1.7-1
connector_id="04t3x000001Guc9AAC";

# products path
import_path="productCatalog/products";
# community path
community_config_path="scripts/8_communityConfig";
# billing connector key
connector_key="enxbil2019!";
# default org duration
duration="30";
# preview (only for V9)
isPreview=false;

while getopts "a:d:i:c:b:e:t:d:l:n:r:" o; do
    case "${o}" in
        a)
            name=${OPTARG}
            ;;
        d)
            duration=${OPTARG}
            ;;
        i)
            import_path=${OPTARG}
            ;;
        c)
            cpq_package=${OPTARG}
            ;;
        b)
            b2b_package=${OPTARG}
            ;;
        e)
            customerflow_id=${OPTARG}
            ;;
        t)
            community_template_id=${OPTARG}
            ;;
        d)
            docgen_id=${OPTARG}
            ;;
        l)
            billing_id=${OPTARG}
            ;;
        n)
            connector_id=${OPTARG}
            ;;
        r)
            isPreview=${OPTARG}
            ;;            
        *)
            ;;
    esac
done

shift $((OPTIND-1))

if [ -z "${import_path}" ]; then
    echo 'IMPORT PATH HAS NOT BEEN GIVEN! ENVIRONMENT WILL BE CREATED WITHOUT IMPORTING PRODUCTS!'
fi

if [ -n "${customerflow_id}" ] ; then
  install_recipe=true
fi

if [ -n "${community_template_id}" ] ; then
  install_community_template=true
fi


echo 'CREATING NEW SCRATCH ORG: '$name
    if [ $isPreview = true ]; 
    then 
        sfdx force:org:create -f config/package-scratch-def.json -a $name --durationdays $duration --setdefaultusername release=Preview
    else
        sfdx force:org:create -f config/package-scratch-def.json -a $name --durationdays $duration --setdefaultusername
    fi

    if [ $? -eq 0 ]; then 
        echo 'NEW SCRATCH ORG CREATED SUCCESSFULLY'
    fi


echo 'Installing CPQ package'
yes y | sfdx force:package:install --package $cpq_package --wait 30 --noprompt

echo 'Deploying manual steps'
sfdx force:mdapi:deploy --deploydir scripts/2_caseRecordTypes/ --wait 1

echo 'Installing B2B package'
yes y | sfdx force:package:install --package $b2b_package --wait 30 --noprompt

echo 'Installing Enxoo Billing package'
sfdx force:package:install -u $name --package $billing_id --wait 5

echo 'Installing Billing Connector'
sfdx force:package:install -u $name --package $connector_id -k $connector_key --wait 5

echo 'Installing Enxoo Documents package'
sfdx force:package:install -u $name --package $docgen_id --wait 5

# assign doc permsets - script in apex: https://enxooteam.atlassian.net/wiki/spaces/DOCGEN/pages/3047542/Installation+Instruction
# assign billing permset - script in apex: https://enxooteam.atlassian.net/wiki/spaces/ECPQKB/pages/1929478368/Recipe+-+Enxoo+Billing+Connector

echo 'PUSHING SOURCE CODE TO AN ORG: '$name
sfdx force:source:push -f
    if [ $? -eq 0 ]; then 
        echo 'SOURCE SUCCESSFULLY PUSHED TO AN ORG'
    fi

echo 'ASSIGNING PERMISSION SETS FOR ADMIN AND USER'
sfdx force:user:permset:assign --permsetname CPQ_Administrator
sfdx force:user:permset:assign --permsetname CPQ_User
sfdx force:user:permset:assign --permsetname B2B_Administrator
sfdx force:user:permset:assign --permsetname B2B_User
sfdx force:user:permset:assign --permsetname B2B_OM_Administrator
sfdx force:user:permset:assign --permsetname B2B_OM_User
sfdx force:user:permset:assign --permsetname B2B_SA_User

echo 'ADDING ADDITIONAL CURRENCIES'
sfdx force:data:tree:import -f scripts/7_testConfig_Package/additionalCurrencies.json

echo 'INIT DATA'
    if [ ! -z "$import_path" ]; then
        echo 'IMPORTER PATH ' $import_path
        sfdx enxoo:cpq:prd:import -p *ALL -d $import_path
    fi


echo 'APPLYING MANUAL STEPS'
sfdx force:apex:execute -f scripts/1_apexSteps/initCustomSettings.apex
sfdx force:apex:execute -f scripts/1_apexSteps/setUserRole.apex
sfdx force:mdapi:deploy --deploydir scripts/4_manualStepsPackage/ --wait 1

echo 'ADDITIONAL TEST CONFIG'
sfdx force:mdapi:deploy --deploydir scripts/7_testConfig_Package/metadata --wait 1
sfdx force:apex:execute -f scripts/7_testConfig_Package/apexMethods/initSettings.apex
sfdx force:user:permset:assign --permsetname B2B_For_Tests
sfdx force:user:permset:assign --permsetname B2B_Customer_User_API

if [ -z "${import_path}" ]; then
    sfdx force:apex:execute -f scripts/1_apexSteps/initData.apex
fi

echo 'LOADING TEST DATA'
sfdx force:apex:execute -f scripts/7_testConfig_Package/apexMethods/changeAutocreateQuote.apex
sfdx force:data:bulk:upsert -s Account -f scripts/X_extendedData/B2BAccounts.csv -i ENXCPQ__TECH_EXTERNAL_ID__c -w 5
sfdx force:data:bulk:upsert -s Contact -f scripts/X_extendedData/B2BContacts.csv -i ENXCPQ__TECH_EXTERNAL_ID__c -w 5
sfdx force:data:bulk:upsert -s enxCPQ__Location__c -f scripts/X_extendedData/Locations.csv -i ENXCPQ__TECH_EXTERNAL_ID__c -w 5
sfdx force:data:bulk:upsert -s enxB2B__POP__c -f scripts/X_extendedData/POPs.csv -i ENXB2B__TECH_EXTERNAL_ID__c -w 5
sfdx force:data:bulk:upsert -s enxCPQ__AccountPricebook__c -f scripts/X_extendedData/AccountPricebook.csv -i ENXCPQ__TECH_EXTERNAL_ID__c -w 5
sfdx force:data:bulk:upsert -s Opportunity -f scripts/X_extendedData/Opportunities.csv -i ENXCPQ__TECH_EXTERNAL_ID__c -w 5
sfdx force:data:bulk:upsert -s Quote -f scripts/X_extendedData/Quotes.csv -i ENXCPQ__TECH_EXTERNAL_ID__c -w 5
sfdx force:data:bulk:upsert -s Opportunity -f scripts/X_extendedData/SyncQuote.csv -i ENXCPQ__TECH_EXTERNAL_ID__c -w 5
sfdx force:data:bulk:upsert -s enxCPQ__Network_Node__c -f scripts/X_extendedData/Nodes.csv -i ENXCPQ__TECH_EXTERNAL_ID__c -w 5
sfdx force:apex:execute -f scripts/7_testConfig_Package/apexMethods/changeAutocreateQuote.apex
sfdx force:data:record:create -u $name -s Account -v \"Name='Test Account 1'\"
echo 'TEST DATA LOADED'

echo 'INSTALL ADDITIONAL PACKAGES FOR COMMUNITY'
if [ $install_recipe = true ]; then
  echo 'Installing recipe: Customer Sales Process Flows'
  sfdx force:package:install -u $name --package $customerflow_id --wait 5
fi

if [ $install_community_template = true ]; then
  echo 'Installing community template'
  sfdx force:package:install -u $name --package $community_template_id --wait 5
  echo 'Creating community...'
  sfdx force:community:create -u $name --name 'Customer Portal' --templatename 'Enxoo Customer Portal' --urlpathprefix customers
  echo 'Community created, wait 5min...'
  sleep 300
  sfdx force:mdapi:deploy -u $name --ignoreerrors --deploydir $community_config_path --wait 30
  sfdx force:community:publish -u $name --name 'Customer Portal'
  echo 'Community deployed!'
fi

echo 'CREATING USER AND GENERATING PASSWORD'
sfdx force:user:password:generate
sfdx force:user:display > userCredentials.txt

echo 'SEND EMAIL NOTIFICATION'
sfdx force:apex:execute -f scripts/9_fibremeScripts/createScratchOrg/sendEmailAfterOrgCreated.apex

echo 'SHOW CREDENTIALS'
sfdx force:user:display
sfdx force:org:open 

echo 'SUCCESSFULLY CREATED AND CONFIGURED SCRATCH ORG WITH CPQ & B2B PACKAGES'
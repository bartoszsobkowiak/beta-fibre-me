#!/bin/bash

# https://aaa-c5-dev-ed.lightning.force.com/lightning/setup/ConnectedApplication/page?address=%2Fapp%2Fmgmt%2Fforceconnectedapps%2FforceAppDetail.apexp%3FretURL%3D%252Fsetup%252FNavigationMenus%252Fhome%26connectedAppId%3D0H40900000095WpCAI%26appLayout%3Dsetup%26tour%3D%26sfdcIFrameOrigin%3Dhttps%253A%252F%252Faaa-c5-dev-ed.lightning.force.com%26sfdcIFrameHost%3Dweb%26nonce%3D8327d09a4c9d2565655e76f7e9512b94ca2e85d91c57311386902766ff81849b%26clc%3D1%26id%3D0Ci09000000PCiA
SFDX_CLIENT_ID="3MVG9SOw8KERNN09HSMa5Z4cgWAeqg3qCT96kdPXuOCrBu55rPe8LBjnfkKPawnls1MqAHyVsoXNZdUlatt8b";
SFDX_USER="admin@beta-fibreme.com";
ALIAS="testProd";
sfdx force:auth:jwt:grant --instanceurl https://test.salesforce.com --clientid $SFDX_CLIENT_ID --jwtkeyfile pipelines/server.key --username $SFDX_USER --setalias $ALIAS

# # Package Ids
# enxCPQID = $3
# enxB2BID = $4

# echo 'Installing Enxoo Commerce (enxCPQ), please wait...'
# sfdx force:package:install --package $enxCPQID  -u sit -w 15
# echo 'Installing  Enxoo for Communications (enxB2B), please wait...'
# sfdx force:package:install --package $enxB2BID  -u sit -w 15

# echo 'Installing  Test Runner, please wait...'
# sfdx force:package:install --package 04t4I000000lFB8QAM  -u sit -w 15

# echo 'Packages successfully installed:'
# sfdx force:package:installed:list

# add_permsets="y"

# if [[ $add_permsets =~ ^[yY]$ ]]; then
# 	echo 'Setting CPQ & B2B permsets...'
# 	sfdx force:user:permset:assign --permsetname CPQ_Administrator -u sit
# 	sfdx force:user:permset:assign --permsetname CPQ_User -u sit
# 	sfdx force:user:permset:assign --permsetname B2B_Administrator -u sit
# 	sfdx force:user:permset:assign --permsetname B2B_User -u sit
# 	sfdx force:user:permset:assign --permsetname B2B_OM_Administrator -u sit
# 	sfdx force:user:permset:assign --permsetname B2B_OM_User -u sit
# 	sfdx force:user:permset:assign --permsetname B2B_SA_User -u sit
# fi

# echo 'Dev org initialized successfully!'
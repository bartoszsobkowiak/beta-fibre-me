from tools.connector import GitConnector
from tools.sfDeployer import sfDeployer
from tools.utils import NoFreeRepo
from git import GitCommandError
from datetime import datetime
from pathlib import Path
import argparse
import json
import sys
import os

success = False


def setup_arg_parser():
    parser = argparse.ArgumentParser(
        description="Script to handle creating package from commits, branches and lists of commits and deploying them to SF sandboxes")

    parser.add_argument('--srcBranch', '-s', type=str, help='The source branch or tag prefix for the comparison, defaults to master')
    parser.add_argument('--mode', '-m', required=True, choices=["branch", "tag"], type=str, help='The mode can be either branch package (Difference between two branches) or tag package (diff between HEAD of selected branch and tag with prefix)')
    parser.add_argument('--currentBranch', '-b', type=str, help='The branch with changes to be deployed')
    parser.add_argument('--gitPath', type=Path, help='Path to folder with repositiories')
    parser.add_argument('--removed', '-r', type=str, help='Elements removed from the deployment')
    parser.add_argument('--packageFolder', '-f', type=str, help='Folder where the package will be prepared, defaults to "package"')
    parser.add_argument('--resultPath', type=str, help='Folder where the deployment results will be stored')
    parser.add_argument('--checkOnly', action='store_true', help='If passed the package will require validation on the Salsforce Deployment Status page')

    # FLAGS FOR FOLDER MANAGEMENT
    parser.add_argument('--packageOnly', action='store_false', help='If passed the package will be created but not deployed')

    # GENERAL CONFIG
    parser.add_argument('--orgName', '-o', type=str, help='Target org name, same as in sfdx config on the server/locally')

    # TEST RELATED ARGS
    parser.add_argument('--noTestClasses', '-n', nargs='*', help='List of classes without tests, as -n class1 -n class2 etc')
    parser.add_argument('--runTest',  help='List of test classes to be passed to sfdx deploy command')
    parser.add_argument('--testLevel',  help='sfdx type run test, with standard choices. This will override any automatic test level detection', choices=['NoTestRun', 'RunSpecifiedTests', 'RunLocalTests', 'RunAllTestsInOrg'])
    parser.add_argument('--extraTests', help="List of extra tests to be run any times tests are run")
    parser.add_argument('--runDetectedTests', action='store_true', help='If passed the list of tests will be detected and run')
    parser.add_argument('--testPackageList', help='If passed the list of tests will be run in a small deployment before main deployment')

    args = parser.parse_args(sys.argv[1:])

    if args.packageOnly and args.orgName is None:
        print("deployHelper => Error - no org name  provided")
        exit(1)

    if args.currentBranch is None and args.mode == 'branchPackage':
        print("deployHelper => Error - no branch for package provided")
        exit(1)

    if args.gitPath is None:
        print("deployHelper => Error - no git repository path provided")
        exit(1)

    return args


args = setup_arg_parser()

packageFolder = args.packageFolder if args.packageFolder is not None else 'package'
srcBranch = args.srcBranch if args.srcBranch is not None else 'master'
elementsRemoved = args.removed.strip('][').split(',') if args.removed is not None and args.removed != 'NONE' else ''

gitLink = GitConnector()

if args.noTestClasses:
    gitLink.classesWithoutTests = args.noTestClasses

try:
    gitLink.getFreeGitRepo(args.gitPath)
except NoFreeRepo as e:
    msg =  'No free repos at %s, already waited for %s minutes, quiting deployment to %s' % (str(datetime.now()) ,str(e.waitTime/60) ,args.orgName)
    print('deployHelper => %s ' % msg)
    exit(1)

packageFolder = gitLink.setPackageFolder(packageFolder, args.packageOnly)
gitLink.masterBranchName = srcBranch
gitLink.branchName = args.currentBranch
gitLink.setRemoved(elementsRemoved)

try:
    gitLink.setGitSetup()
except GitCommandError as e:
    print('Git error while deploying to ' + args.orgName + ' - unable to pull from repo' if args.orgName is not None else 'Git error - unable to pull from repo')
    exit(1)

gitLink.initialCleanup()

if args.mode == 'branch':
    print('deployHelper => Creating diff between ' + args.currentBranch + ' and ' + srcBranch)
    gitLink.buildBranchPackage()

elif args.mode == 'tag':
    print('deployHelper => Looking for tag with prefix ' + srcBranch)
    tag = gitLink.getLastTagForPrefix(srcBranch)
    if tag is None:
        print("deployHelper => Error, no tag was found with prefix: " + args.srcBranch)
        exit(1)

    print('deployHelper => Found tag: ' + tag)
    srcBranch = tag
    gitLink.masterBranchName = srcBranch

    print('deployHelper => Creating diff between ' + args.currentBranch + ' and ' + srcBranch)
    gitLink.buildBranchPackage()

gitLink.removePackageElements()
gitLink.setUpProject()

if not args.packageOnly:
    print("deployHelper => Package only mode - exiting without deployment")
    exit(0)

deployer = sfDeployer(packageFolder)
deployer.orgName = args.orgName

testList = ""
if args.checkOnly:
    deployer.checkOnly = True

if args.testPackageList:
    deployer.testPackageList = args.testPackageList

if args.runDetectedTests:
    [testClasses, testList, apexClasses] = gitLink.getTestClasses()
    print('deployHelper => Test classes results: ' + str(testClasses) + ' \n\tTest classes to be run ' + str(testList) + ' \n\t Apex classes without tests in package' + str(apexClasses))

if args.runTest:
    testList = args.runTest

if args.extraTests:
    deployer.extraTests = args.extraTests

deployer.setTestLevel(testList, args.testLevel)

[deployResult, success] = deployer.deploy()

try:
    folderPath = args.resultPath
    dt_string = datetime.now().strftime("%d_%m_%Y_%H:%M:%S")
    filePath = folderPath + args.mode + '_' + dt_string + '.json'
    if not os.path.isdir(folderPath):
        os.makedirs(folderPath)
    with open(filePath,'x') as f:
        f.write(deployResult)
except Exception as e:
    pass

if success:
    print('Deployment Successful')
    exit(0)
else:
    print(deployResult)
    print('Deployment Failed')
    exit(1)

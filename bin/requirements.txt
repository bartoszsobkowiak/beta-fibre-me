gitpython==3.1.7
requests==2.24.0
sfdclib==0.2.26
six==1.15.0
httplib2==0.19.0
argparse==1.4.0
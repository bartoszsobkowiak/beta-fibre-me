from sfdclib import SfdcSession, SfdcMetadataApi, SfdcBulkApi, SfdcToolingApi
from pathlib import Path
from datetime import datetime

import requests
import shutil
import atexit
import time
import json
import sys
import os


class NoFreeRepo(Exception):
    """
    Excpetion raised if there are no free repos in the provided folders.
    Free means without lock.git file present
    """
    def __init__(self, waitTime):
        self.waitTime = waitTime


class TypeOfFile(object):

    typeClass = 'classes'
    typeLwc = 'lwc'
    typePage = 'pages'
    typeAura = 'aura'
    typeEntitlementProcess = 'entitlementProcesses'
    typeObject = 'objects'
    typeTest = 'testes'
    typeMeta = 'meta'
    sfdxRootPath = 'force-app/main/default'
    rootClassesPath = os.path.join(sfdxRootPath ,typeClass)

    @staticmethod
    def getType(filePath):

        if ('default/objects' in filePath):
            return TypeOfFile.typeObject

        if ('default/entitlementProcesses' in filePath):
            return TypeOfFile.typeEntitlementProcess

        if ('default/lwc' in filePath):
            return TypeOfFile.typeLwc

        if('meta.xml' in filePath):
            return TypeOfFile.typeMeta

        if('Test.cls' in filePath):
            return TypeOfFile.typeTest

        if ('default/classes' in filePath):
            return TypeOfFile.typeClass

        if ('default/pages' in filePath):
            return TypeOfFile.typePage

        if ('default/aura' in filePath):
            return TypeOfFile.typeAura

        return None

    @staticmethod
    def isClassType(filePath):
        return TypeOfFile.getType(filePath) == TypeOfFile.typeClass

    @staticmethod
    def isLwcType(filePath):
        return TypeOfFile.getType(filePath) == TypeOfFile.typeLwc

    @staticmethod
    def isPageType(filePath):
        return TypeOfFile.getType(filePath) == TypeOfFile.typePage

    @staticmethod
    def isAuraType(filePath):
        return TypeOfFile.getType(filePath) == TypeOfFile.typeAura

    @staticmethod
    def isObjectType(filePath):
        return TypeOfFile.getType(filePath) == TypeOfFile.typeObject

    @staticmethod
    def isTestType(filePath):
        return TypeOfFile.getType(filePath) == TypeOfFile.typeTest

    @staticmethod
    def isMetaType(filePath):
        return TypeOfFile.getType(filePath) == TypeOfFile.typeMeta

    @staticmethod
    def isEntitlementProcessType(filePath):
        return TypeOfFile.getType(filePath) == TypeOfFile.typeEntitlementProcess


class Configurable():
    SLEEPTIMEOUT = 1
    gitWaitTime = 30
    maxWaittimeForRepo = 600
    openFiles = []
           
    @staticmethod
    def list_files(startpath):
        """Lists all files in the directory and subdirectories of provided startpath"""
        for root, dirs, files in os.walk(startpath):
            fullpath = root.replace(str(startpath), '')
            if fullpath.startswith('/.sfdx'):
                continue

            level = root.replace(str(startpath), '').count(os.sep)
            indent = ' ' * 4 * (level)
            print('{}{}/'.format(indent, os.path.basename(root)))
            subindent = ' ' * 4 * (level + 1)
            for f in files:
                if f != 'sfdx-project.json':
                    print('{}{}'.format(subindent, f))

    @staticmethod
    def addSlash(path):
        if path[-1] != '/':
            path += '/'
        return path

    @staticmethod
    def removeIfExists(path):
        if os.path.exists(path):
            if os.path.isdir(path):
                shutil.rmtree(path)
                return path
            else:
                os.remove(path)
                return path
        else:
            return -1

    @staticmethod
    def freeUpDirectory(path):
        Configurable.removeIfExists(str(path))
    
    @staticmethod
    def getFreeGitDirectory(gitReposFolder, totalTime = 0):
        if os.path.exists(os.path.join(gitReposFolder,'.git')):
            return Path(gitReposFolder)
        gitReposFolder = Configurable.addSlash(str(gitReposFolder))
        repos = sorted(os.listdir(gitReposFolder))
        for repo in repos:
            if not os.path.exists(gitReposFolder + repo + '/lock.git'):
                open(gitReposFolder + repo + '/lock.git', 'a').close()
                atexit.register(Configurable.freeUpDirectory, path=gitReposFolder + repo + '/lock.git' )
                print('Configurable => Git repository path for all operations : ' + str(gitReposFolder + repo))
                return Path(gitReposFolder + repo)
        if totalTime > Configurable.maxWaittimeForRepo:
            raise NoFreeRepo(totalTime)

        print('Configurable => No free repos at ' + str(datetime.now()) + ' waiting for ' + str(Configurable.gitWaitTime) + ' seconds, then retrying')
        time.sleep(Configurable.gitWaitTime)
        return Configurable.getFreeGitDirectory(gitReposFolder, totalTime + Configurable.gitWaitTime)

    @staticmethod
    def getTempFolder(parentFolder = '.', deleteAfterwards = True):
       
        if not os.path.exists(parentFolder):
            os.makedirs(parentFolder)
            if deleteAfterwards:
                atexit.register(Configurable.freeUpDirectory, path=parentFolder)
            print('Configurable => Top level folder for temp folder does not exists, creating tree')
        temps = sorted(os.listdir(parentFolder))
        i = 0
        while str(i) in temps:
            i += 1
        folder = os.path.join(parentFolder, str(i))
        os.mkdir(folder)
        if deleteAfterwards:
            atexit.register(Configurable.freeUpDirectory, path=folder)
        print('Configurable => Temp folder created at : ' + str(folder))
        return Path(folder) 
       

    def openFile(self, path):
        try:
            f = open(path, 'r')
            self.openFiles.append(f)
            return f.read()
        except Exception as e:
            print("Configurable => Error opening " + str(path) + " file" + str(e))
            raise

    def closeFiles(self):
        for f in self.openFiles:
            f.close()

    @staticmethod 
    def getConfigFile(configFilePath):
        # Load Local Config File
        try:
            config = json.loads(open(configFilePath, 'r').read())
            localConfig = config
        except ValueError as e:
            print('Configurable => Couldn\'t load local config, JSON malformed, error : ' + str(e))
            raise

        # Get Global Config and merge with local
        if "globalConfigPath" in config:
            globalConfigPath = config["globalConfigPath"]
            del config["globalConfigPath"]
            try:
                globalConfig = json.loads(open(globalConfigPath, 'r').read())
                config = globalConfig
                config.update(localConfig)
            except ValueError as e:
                print('Configurable => Couldn\'t load global config, JSON malformed, error : ' + str(e))
        else:
            print('Configurable => No global config provided, skipping global config retrieval')
        
        return config

    @staticmethod 
    def getConfig(configFilePath, parser):
        config = Configurable.getConfigFile(configFilePath)
        try:
           
            argList = []
            for arg in config:
                if arg == 'flagList':
                    argList.extend(config[arg])
                elif isinstance(config[arg], str):
                    argList.append(arg)
                    argList.append(config[arg])
                else:
                    argList.extend(arg)
                    argList.extend(config[arg])
            print(str(argList))
            argList.extend(sys.argv[1:])

            args = parser.parse_args(argList)


            ### Get universal config data
            # Get root folder for all operations - all other paths should be relative to root
            if not args.root:
                args.root = './'
            args.root = Path(args.root)
            print('Configurable => Root folder for all operations : ' + str(args.root.absolute()))

        except (ValueError, TypeError) as e:
            print('Configurable => Warrning : config incomplete, errors while retrieving : ' + str(e))
        
    @staticmethod
    def getGlobalRegex(config):
        try:
            globalConfig = json.loads(open(config["globalConfigPath"], 'r').read())
            globalregexJSON = json.loads(
                open(globalConfig['globalProfilesConfig'], 'r').read())
            regexJSON = json.loads(open(config['profilesConfig'], 'r').read())
            for key in list(regexJSON['profilePermissionsMap'].keys()):
                for category in list(regexJSON['profilePermissionsMap'][key].keys()):
                    try:

                        globalList = globalregexJSON['profilePermissionsMap'][key][category]
                        localList = regexJSON['profilePermissionsMap'][key][category]
                        localList = [localList] if isinstance(
                            localList, str) else localList
                        globalList = [globalList] if isinstance(
                            globalList, str) else globalList
                        localList.extend(globalList)
                        localList = list(set(localList))
                        regexJSON['profilePermissionsMap'][key][category] = localList
                    except KeyError as e:
                        print("Configurable=> No key " + str(category) + ' or ' + str(key) + ' in one of the configs. Error: ' + str(e))

            return regexJSON
        except Exception as e:
            print('Configurable => Error getting global profile, or updating profile config : ' + str(e))

    @staticmethod
    def getLastCommitHash(targetFile):
        try:
            with open(targetFile, 'r') as hexFile:
                currentBranchCommitHex = hexFile.read()
                print("Configurable => last commit from previous deployment: " + currentBranchCommitHex)
                return currentBranchCommitHex
        except OSError as e:
            print('Configurable =>  Error loading last commit hash : ' + str(e))
            print('Root directory for hash : ' + str(os.getcwd()))
            return -1


class Connectable(Configurable):

    def __init__(self, username, password, token):
            try:
                self.username = username
                self.password = password
                self.token = token
                s = SfdcSession(username, password, token, True)
                s.login()
                self.metadata = SfdcMetadataApi(s)
                self.bulk = SfdcBulkApi(s)
                self.tooling = SfdcToolingApi(s)
                self.metadata = SfdcMetadataApi(s)
                if self.authorizeSF() == 0:
                    self.loggedIn = True
                else:
                    self.loggedIn = False
            except Exception as e:
                #  print('Connectable => Can\'t log into SF :' + str(e))
                self.loggedIn = False
       
    def authorizeSF(self, consumerId, consumerSecret ):
        params = {
            "grant_type": "password",
            # "client_id": self.consumerId,  # Consumer Key #TODO Check if this is needed
            # "client_secret": self.consumerSecret,  # Consumer Secret
            "username": self.username,  # The email you use to login
            # Concat your password and your security token
            "password": self.password + self.token
        }

        r = requests.post("https://test.salesforce.com/services/oauth2/token", params=params)
        self.authResp = r
        print("Connectable => Response : " + str(r))
        if r.status_code < 300:
            print("Connectable => Logged In sucessfully")
            return 0
        else:
            print("Connectable => Authorization rejected with response code " + r.status_code)
            return 1
        self.access_token = r.json().get("access_token")
        self.instance_url = r.json().get("instance_url")
        print("Connectable => Access Token:", self.access_token)
        print("Connectable => Instance URL", self.instance_url)

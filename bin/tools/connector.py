from tools.utils import Configurable, Connectable, TypeOfFile
from datetime import datetime, timedelta
import xml.etree.ElementTree as ET
from git import Git, Repo, GitCommandError
from pathlib import Path
import shutil
import errno
import glob
import time
import os
import re


class GitConnector(Connectable):
    NSMAP = {"md": "http://soap.sforce.com/2006/04/metadata"}

    def __init__(self, username = '', password = '', token = ''):
        self.authors = {}
        self.skippedList = []
        self.affectedTests = []
        self.processed = []
        self.classes = []
        self.removedClasses = []
        self.removedPages = []
        self.removed = []
        self.classesWithoutTests = []
        self.ignorePullError = False
        super().__init__(username, password, token)

    ###########################################
    #   Setters for all attributes to look nice for selective changes

    def setPackageFolder(self, packageFolder, cleanUp = True):
        self.gitPackageFolder = Configurable.getTempFolder(Path(packageFolder).absolute(),cleanUp)
        self.package = self.gitPackageFolder
        return self.package
        
    def getFreeGitRepo(self, gitPath):
        self.gitPath = Configurable.getFreeGitDirectory(gitPath)
        
    def saveCommitInfo(self):
        currentBranchCommitHex = self.repo.commit(self.branchName).hexsha
        if hasattr(self, "hexFile"):
            with open(self.hexFile, "w+") as hexFile:
                hexFile.write(currentBranchCommitHex)

    def setRemoved(self, removed):
        newRemoved = [] # TODO add glob here for simple regex
        for path in removed:
            if TypeOfFile.sfdxRootPath in path:
                newRemoved.append(path)
            else:
                newRemoved.append(TypeOfFile.sfdxRootPath + "/" + path)
        self.removed = newRemoved
    ##############################

    # This can be used to safely checkout branch in git
    def checkoutBranchSafe(self, branch, ignorePullError = True): #TODO Replace all checkouts with this function
        try:
            time.sleep(self.SLEEPTIMEOUT)
            self.origin.fetch()
            time.sleep(self.SLEEPTIMEOUT)
            self.repo.git.checkout(branch)
            time.sleep(self.SLEEPTIMEOUT)
            self.origin.pull()
        except GitCommandError as e:
            try:
                print("Connector => Checking: " + str(branch))
                self.repo.git.reset("--hard")
            except GitCommandError as e:
                print("Connector => Reset failed")
                if not ignorePullError:
                    raise GitCommandError("reset", 1)

    # This set's up all necessary git stuff:
    #   1)Set's up repo and remote
    #   2)Fetches and pulls new data
    #   3)Resets if not possible
    #   4)Throws error if can't do either, unless ignorePullError is set
    #
    def setGitSetup(self, gitShell = None, repo = None):
        self.repo = repo or Repo(self.gitPath)
        self.git = gitShell or Git(self.gitPath)
        self.origin = self.repo.remote()
        try:
            time.sleep(self.SLEEPTIMEOUT)
            self.origin.fetch()
            time.sleep(self.SLEEPTIMEOUT)
            self.repo.git.checkout(self.branchName)
            time.sleep(self.SLEEPTIMEOUT)
            self.origin.pull()
        except GitCommandError as e:
            try:
                print("Connector => Checking: " + str(self.branchName))
                self.repo.git.reset("--hard")
            except GitCommandError as e:
                print("Connector => Reset failed")
                if not self.ignorePullError:
                    raise GitCommandError("reset", 1)
        try:
            time.sleep(self.SLEEPTIMEOUT)
            self.origin.fetch()
            time.sleep(self.SLEEPTIMEOUT)
            self.repo.git.checkout(self.masterBranchName)
            time.sleep(self.SLEEPTIMEOUT)
            self.origin.pull()
        except GitCommandError as e:
            try:
                print("Connector => Checking: " + str(self.branchName))
                self.repo.git.reset("--hard")
            except GitCommandError as e:
                print("Connector => Reset failed")
                if not self.ignorePullError:
                    raise GitCommandError("reset", 1)

    #Copy the project files from gitRepository`
    def setUpProject(self):
        try:
            sfdxProjectPath = self.package.joinpath('sfdx-project.json')
            sfdxGitPath = self.gitPath.joinpath('sfdx-project.json')

            sfdxFolderPath = self.package.joinpath('.sfdx')
            sfdxFolderGitPath = self.gitPath.joinpath('.sfdx')
            if not sfdxProjectPath.exists():
                if sfdxGitPath.exists():
                    shutil.copyfile(sfdxGitPath, sfdxProjectPath)
                else:
                    print('Connector => No project file in git repo to copy')
                    
            if not sfdxFolderPath.is_dir():
                if sfdxFolderGitPath.exists():
                    shutil.copytree(sfdxFolderGitPath, sfdxFolderPath)
                else:
                    print('Connector => No .sfdx folder in git repo to copy')
                    
        except Exception as e:
            print('Connector => Error copying project files : ' + str(e))

    #  Delete package folder contents, put all necessary config here
    def initialCleanup(self):
        Configurable.removeIfExists(os.path.join(self.package, TypeOfFile.sfdxRootPath))

    # Creates and returns new branch branched out from self.branchName or passed orignBranch with provided name
    def createNewBranch(self, branchName, originBranch = None):
        try:
            originBranch = originBranch or self.branchName
            time.sleep(self.SLEEPTIMEOUT)
            self.origin.fetch()
            time.sleep(self.SLEEPTIMEOUT)
            self.repo.git.checkout(originBranch)
            time.sleep(self.SLEEPTIMEOUT)
            self.origin.pull()
        except GitCommandError as e:
            try:
                print(
                    "Connector => Error pulling branch "
                    + str(originBranch)
                    + ", Error message : "
                    + str(e)
                    + "\tTrying to reset instead"
                )
                self.repo.git.reset("--hard")
            except GitCommandError as e:
                print("Connector => Reset failed, exiting")
                raise GitCommandError("reset", 1)
        newBranch = self.repo.create_head(branchName)
        newBranch.checkout()
        return newBranch

    def getLastTagForPrefix(self, prefix):
        last_matched = None

        tags = self.repo.git.tag().split('\n')
        if len(tags) > 0:
            for tag in tags:
                if tag.startswith(prefix):
                    last_matched = tag

        return last_matched

    # Get's diff from branches in connector object (set before) and creates package
    def buildBranchPackage(self):
        self.getBranchDiff()
        self.createPackage()
        self.getTestClasses()

    # Get's diff from provided commit hash and creates package
    def buildCommitPackage(self, commitHash):
        self.repo.git.checkout(self.branchName)
        self.repo.git.reset("--hard")

        commit = self.repo.commit(commitHash)
        for path in commit.stats.files:
            if (
                commit.stats.files[path]["deletions"]
                < commit.stats.files[path]["lines"]
            ):
                self.copyFile(self.package, path)
                if TypeOfFile.getType(path) == TypeOfFile.typeClass:
                    self.classes.append(path)

    # Get's all commits until provided hash, WITHOUT THE LAST ONE
    def getCommitListUntil(self, commitHash):
        commitList = []
        fullList = []
        currentBranchCommit = self.repo.commit(self.branchName)
        fullList.append(currentBranchCommit)
        fullList.extend(currentBranchCommit.iter_parents())
        for commit in fullList:
            if commit.hexsha == commitHash:
                break
            commitList.append(commit)
        return commitList

    #  create diff from provided merge base and branch passed into connector as branchName. diff is stored as object variable
    def getMergeBaseDiff(self, mergeBase):
        print("Creating diff between %s and %s " % (self.branchName, mergeBase))
        try:
            self.repo.git.checkout(self.branchName)
        except GitCommandError as e:
            try:
                print(
                    "Connector => Error pulling branch "
                    + str(self.branchName)
                    + ", Error message : "
                    + str(e)
                    + "\ttrying to reset instead"
                )
                self.repo.git.reset("--hard")
            except GitCommandError as e:
                print("Connector => Reset failed, exiting")
                raise GitCommandError("reset", 1)

        currentBranch = self.repo.commit(self.branchName)
        masterBranch = self.repo.commit(mergeBase)

        self.diff = masterBranch.diff(currentBranch)

    #  create diff from branches passed into connector as masterBranch and branchName. diff is stored as object variable. if True is passed, masterBranch will be merged into branchName beforehand

    def getBranchDiff(self, merge=False):
        #  Checkout both branches and pull changes
        self.repo.git.fetch()

        time.sleep(self.SLEEPTIMEOUT)
        self.repo.git.checkout(self.masterBranchName)

        time.sleep(self.SLEEPTIMEOUT)
        self.repo.git.reset("--hard")
        try:
            self.repo.git.checkout(self.branchName)
        except GitCommandError as e:
            try:
                print(
                    "Connector => Error pulling branch "
                    + str(self.branchName)
                    + ", Error message : "
                    + str(e)
                    + "\ttrying to reset instead"
                )
                self.repo.git.reset("--hard")
            except GitCommandError as e:
                print("Connector => Reset failed, exiting")
                raise GitCommandError("reset", 1)

        time.sleep(self.SLEEPTIMEOUT)
        self.repo.git.reset("--hard")
        if merge:
            self.repo.git.merge(self.masterBranchName)

        #  Diff the branches against each other
        masterBranch = self.repo.commit(self.masterBranchName)
        currentBranch = self.repo.commit(self.branchName)
        try:
            self.diff = self.repo.merge_base(currentBranch, masterBranch)[0].diff(currentBranch)
        except Exception as e:
            print('GitConnector => Couldn\'t get merge base diff, error: ' + str(e))
            exit(1)

    # Copy files to create package using diff member of Connector
    def createPackage(self, createPackageFromClassOnly=False):
        self.destructiveChangesList = []

        for fileDiff in self.diff:
            # Get the correct path and type of file
            path = str(fileDiff.b_rawpath).split("'")[1]
            fileType = TypeOfFile.getType(path)
            isClassType = fileType == TypeOfFile.typeClass

            # Add to list of destruvtive changes - currently not used, info only
            if fileDiff.change_type == "D":
                self.destructiveChangesList.append(path)
                

            elif isClassType or fileType == TypeOfFile.typeMeta:
                self.copyFile(self.package, path)
                if isClassType:
                    self.classes.append(path)
                

            elif not createPackageFromClassOnly:
                self.copyFile(self.package, path)

    # removes elemnets set as remvoed in object from package
    def removePackageElements(self):
        removed = []
        try:
            if self.removed is not None:
                for filePath in self.removed:
                    filePath = (
                        filePath
                        if TypeOfFile.sfdxRootPath in filePath
                        else TypeOfFile.sfdxRootPath + "/" + filePath
                    )
                    filePath = str(self.package) + "/" + filePath
                    fileList = glob.glob(filePath)
                    for filePath in fileList:
                        if Path.exists(Path(filePath)):
                            if "main/default/" in filePath:
                                path = Configurable.removeIfExists(filePath)
                                if path != -1:
                                    path = path.split("main/default/")[1]
                                    Configurable.removeIfExists(filePath + "-meta.xml")
                            else:
                                path = Configurable.removeIfExists(filePath)
                                Configurable.removeIfExists(filePath + "-meta.xml")
                            path = re.sub(r"[\\]+", "/", path)
                            if path != -1:
                                removed.append(path)
            removedMap = {}
            for element in removed:
                try:
                    elements = element.split("/")
                    if len(elements) == 1:
                        removedMap[elements[0]] = " - Whole folder"
                        continue
                    if len(elements) > 2:
                        removedMap[elements[0]] = (
                            elements[1] + " - " + str(elements[2:])
                        )
                    else:
                        removedMap[elements[0]] = elements[1]
                except (IndexError, KeyError) as e:
                    print(
                        "Connector => Error adding "
                        + element
                        + " to removed map : "
                        + str(e)
                    )
            self.removed = removedMap
        except OSError as e:
            print("Connector => Error removing elements from package")
            if filePath is not None:
                print("Connector => Erroneous path : " + str(filePath))
            raise
    # Generall git log --pretty wrapper
    def getLogPretty(self, path, element):
        
        try:
            cwd = os.getcwd()
            os.chdir(self.gitPath)
            if TypeOfFile.sfdxRootPath not in path and  TypeOfFile.sfdxRootPath.replace('/','\\') not in path:
                path = os.path.join(TypeOfFile.sfdxRootPath, path)
            return_val = self.git.log("--pretty=" + element, "--follow", "--", path, n=1)
            os.chdir(cwd)
        except OSError as e:
            print('Connector => Error while getting git log : %s' % e)
            return_val = ''
        return return_val

    # Get's last file commiter and appends some emotes for fun
    def getLastCommiter(self, path):
        author = self.getLogPretty(path, '%aN')
        if author in self.authors:
            author = self.authors[author]
        return author

    # Get's last file commit
    def getLastCommitHash(self, path):
        return self.getLogPretty(path, '%H')

    # Gets last time a file was commited
    def getDateCommited(self, path):
        dateTime = self.git.log("--pretty=%ct", "--follow", "--", path, n=1)
        return int(dateTime)

    # Helper to copy all directory
    def copyDir(self, src, dst, symlinks=False, ignore=None):
        if os.path.isdir(src):
            for item in os.listdir(src):
                s = src
                if s[-1] != "/":
                    s += "/"
                s += item
                d = dst
                if d[-1] != "/":
                    d += "/"
                d += item
                if os.path.isdir(s):
                    if not os.path.isdir(d):
                        shutil.copytree(s, d)
                    self.copyDir(s, d, symlinks, ignore)
                else:
                    shutil.copy2(s, d)
        else:
            shutil.copy2(src, dst)

    def getTimeLastEditedInOrg(self, name):
        query = "Select Id, Name, LastModifiedDate From SlaProcess Where Name = "
        query += "'" + name.split("_v")[0] + "'"
        result = self.bulk.export("SlaProcess", query)
        try:
            lastEditDate = result.split("\n")[1].split(",")[2].replace('"', "")
            lastEditDate = datetime.strptime(lastEditDate, r"%Y-%m-%dT%H:%M:%S.000Z")
            return int(lastEditDate.timestamp())

        except IndexError as e:
            print(
                "Connector => GitConnector => Can't find last edited date for "
                + name
                + " with error  "
                + str(e)
            )
            return -1

    # Helper to copy file with some extra steps needed for deployment
    def copyFile(self, target, filePath):

        # Get absolute paths to the source file and destination
        srcPath = os.path.join(self.gitPath, filePath)
        destPath = os.path.join(target, filePath)

        # Check if folder structure exists, create if not
        if not os.path.exists(os.path.dirname(destPath)):
            try:
                os.makedirs(os.path.dirname(destPath))
            except OSError as exc:  # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise
        fileType = TypeOfFile.getType(srcPath)  # Get the type of file
        if fileType == TypeOfFile.typeEntitlementProcess:
            if self.loggedIn:

                # Get the folder structure of copying object
                def getRootFolder(path):
                    return path.split("default")[0] + "default/"

                _, typePath, elementName, *_ = srcPath.split("default")[1].split("/")

                # Get the xml definition for deploying object
                srcPathXMLObject = (
                    getRootFolder(srcPath)
                    + typePath
                    + "/"
                    + elementName
                    + "/"
                    + elementName
                    + ".object-meta.xml"
                )
                destPathXMLObject = (
                    getRootFolder(destPath)
                    + typePath
                    + "/"
                    + elementName
                    + "/"
                    + elementName
                    + ".object-meta.xml"
                )

                # Get the folder paths of object
                srcFolderPath = getRootFolder(srcPath) + typePath + "/" + elementName
                destFolderPath = getRootFolder(destPath) + typePath + "/" + elementName

                commitDate = self.getDateCommited(
                    "force-app/" + srcPath.split("force-app/")[1]
                )
                try:
                    elementName = elementName.split(".")[0]
                except IndexError as e:
                    print(
                        "Connector => Error while spliting entitlement process element name, message name : "
                        + str(e)
                    )
                editDate = self.getTimeLastEditedInOrg(elementName)
                if editDate != -1 and editDate < commitDate:
                    epXML = ET.parse(open(srcPath, "r"))
                    epXMLRoot = epXML.getroot()
                    ET.register_namespace("", self.NSMAP["md"])
                    version = int(
                        epXMLRoot.findall(".//md:versionNumber", namespaces=self.NSMAP)[
                            0
                        ].text
                    )
                    newVersion = version + 1
                    epXMLRoot.findall(".//md:versionNumber", namespaces=self.NSMAP)[
                        0
                    ].text = str(newVersion)
                    epXML.write(destPath.replace(str(version), str(newVersion)))
                else:
                    os.remove(destPath)
            else:
                if os.path.exists(srcPath) and not os.path.exists(destPath):
                    shutil.copy(srcPath, destPath)
        if fileType == TypeOfFile.typeObject:

            # Get the folder structure of copying object
            def getRootFolder(path):
                return path.split("default")[0] + "default/"

            _, typePath, elementName, *_ = srcPath.split("default")[1].split("/")

            # Get the xml definition for deploying object
            srcPathXMLObject = (
                getRootFolder(srcPath)
                + typePath
                + "/"
                + elementName
                + "/"
                + elementName
                + ".object-meta.xml"
            )
            destPathXMLObject = (
                getRootFolder(destPath)
                + typePath
                + "/"
                + elementName
                + "/"
                + elementName
                + ".object-meta.xml"
            )

            # Get the folder paths of object
            srcFolderPath = getRootFolder(srcPath) + typePath + "/" + elementName
            destFolderPath = getRootFolder(destPath) + typePath + "/" + elementName

            # Copy current element
            if os.path.exists(srcPath) and not os.path.exists(destPath):
                shutil.copy(srcPath, destPath)

            try:
                if not (Path.exists(Path(destPathXMLObject))):
                    shutil.copy2(srcPathXMLObject, destPathXMLObject)
            except Exception as e:
                print("Connector => Cannot copy object XML file, error: " + str(e))
                raise

            if srcPath not in self.processed:
                # Add the object to already processed for master detail so that we limit unnecessary copying
                self.processed.append(srcPath)
                # Create folder for fields if nonexistent
                if not os.path.exists(destFolderPath + "/fields"):
                    try:
                        os.makedirs(destFolderPath + "/fields")
                    except OSError as exc:  # Guard against race condition
                        if exc.errno != errno.EEXIST:
                            raise
                # Copy all the fields with master detail
                for root, __, files in os.walk(
                    getRootFolder(srcPath) + typePath + "/" + elementName + "/fields"
                ):
                    for field in files:
                        try:
                            fieldPath = Path(os.path.join(root, field))
                            fieldXMLRoot = ET.parse(
                                open(fieldPath, "r", encoding="utf-8")
                            ).getroot()
                            ET.register_namespace("md", self.NSMAP["md"])
                            types = fieldXMLRoot.findall(
                                ".//md:type", namespaces=self.NSMAP
                            )
                            if len(types) > 0:
                                result = types[0].text
                                if result == "MasterDetail":
                                    shutil.copy2(fieldPath, destFolderPath + "/fields")
                        except (IndexError, UnicodeDecodeError) as e:
                            # If the field reading throws error, copy to be on the safe side
                            print(
                                "Connector => GitConnector => error while parsing field file :"
                                + str(fieldPath)
                                + " - reading error \n !!! Field "
                                + str(fieldPath)
                                + " added to package since we couldn't check for MasterDetail condition"
                            )
                            shutil.copy2(fieldPath, destFolderPath + "/fields")
                        except Exception as e:
                            print(
                                "GitConnector => other error while parsing field file :"
                                + fieldPath
                            )
            return
        if fileType in [TypeOfFile.typeAura, TypeOfFile.typeLwc]:
            # For LWC and Aura copy entire folders

            def getRootFolder(path):
                return path.split("default")[0] + "default/"

            _, typePath, elementName, *_ = srcPath.split("default")[1].split("/")

            # Get the folder paths of object
            srcFolderPath = getRootFolder(srcPath) + typePath + "/" + elementName
            destFolderPath = getRootFolder(destPath) + typePath + "/" + elementName

            # Check to not copy multiple times
            if srcFolderPath not in self.processed:
                self.processed.append(srcFolderPath)
                self.copyDir(srcFolderPath, destFolderPath)
            return

        if os.path.exists(srcPath) and not os.path.exists(destPath):
            shutil.copy(srcPath, destPath)
            metaSuffix = "-meta.xml"
            lengthOfMetaSuffix = len(metaSuffix)

            if metaSuffix not in srcPath:
                if os.path.exists(srcPath + metaSuffix):
                    shutil.copy(srcPath + metaSuffix, destPath + metaSuffix)

            else:
                if os.path.exists(srcPath[:-lengthOfMetaSuffix]):
                    shutil.copy(
                        srcPath[:-lengthOfMetaSuffix], destPath[:-lengthOfMetaSuffix]
                    )
            return

    # Get a list of test classes, apex classes and string to be passed to sfdx command
    def getTestClasses(self):
        testClasses = []
        apexClassesWithoutTests = []
        testClassListString = ""
        # print("Test classes : %s" % ",".join(self.classes))

        for filePath in self.classes:
            # print("File path checked for tests : %s" % filePath)
            fileName = filePath.split("/")[-1]
            testPath = str(self.gitPath) + "/" + filePath.replace(".cls", "Test.cls")
            pathExists = os.path.exists(testPath)
            toSkip = False
            for skip in self.classesWithoutTests:
                if skip in fileName:
                    toSkip = True
            if toSkip:
                continue
            if "Test" in fileName:
                pathToAdd = fileName.split(".")[0]
                testClasses.append(pathToAdd)
                if pathToAdd not in testClassListString and pathExists:
                    testClassListString += pathToAdd + ","
            elif pathExists:
                pathToAdd = fileName.split(".")[0] + "Test"
                testClasses.append(pathToAdd)
                if pathToAdd not in testClassListString and pathExists:
                    testClassListString += pathToAdd + ","
            else:
                apexClassesWithoutTests.append(fileName.split(".")[0])


        testClassListString = testClassListString[0:-1]

        self.testClassList = testClasses
        self.testClassListString = testClassListString
        self.apexClassesWithoutTestsList = apexClassesWithoutTests
        return [testClasses, testClassListString, apexClassesWithoutTests]

    def buildNDepthPackage(self, depth):
        commitList = []
        fileSet = set()
        head = self.repo.commit(self.branchName)
        i = 0
        totalDepth = depth
        while depth:
            i = i + 1
            print('Connector => Processing commit %i out of %i ( %i left )' % (i,totalDepth,depth))
            depth -= 1
            commitList.append(head)
            head = head.parents[0]
            for path in head.stats.files:
                if (
                    head.stats.files[path]["deletions"]
                    < head.stats.files[path]["lines"]
                ) and path not in fileSet:
                    fileSet.add(path)
        i = 0
        for file_path in fileSet:
            i = i + 1
            print('Connector => Processing file ', str(i), ' out of ', str(len(fileSet)))
            # Get the correct path and type of file
            fileType = TypeOfFile.getType(file_path)
            isClassType = fileType == TypeOfFile.typeClass
            if isClassType or fileType == TypeOfFile.typeMeta:
                self.copyFile(self.package, file_path)
                if isClassType:
                    self.classes.append(file_path)
                
            else:
                self.copyFile(self.package, path)

    # Build multiple packages for syncs
    def buildSyncPackage(self, syncMap, folderMap, depth):
        for branch in syncMap:
            packageFolder = folderMap[branch]
            Configurable.removeIfExists(packageFolder)
            os.mkdir(packageFolder)
            mergeCommitList = []
            head = self.repo.commit(branch)
            while depth:
                if "Merge branch 'master'" in head.message:
                    depth -= 1
                    mergeCommitList.append(head)
                head = head.parents[0]
            print('Connector => Sync package created up to commit ' + str(head.hexsha) + ' with message ' + str(head.message))
            self.repo.git.checkout(branch)
            self.branchName = branch
            self.package = packageFolder
            for commit in mergeCommitList:
                self.buildCommitPackage(commit.hexsha)
            self.removePackageElements()
            self.setUpProject()



import os
import subprocess
import json
from pathlib import Path
from tools.utils import TypeOfFile, Configurable
import time


class sfDeployer():
    
    
    def __init__(self, packageFolder ):
        self.packageFolder = packageFolder

        try:
            self.packageFolder = Path(packageFolder).absolute()
        except Exception as e:
            print('Deployer => Error with cwd : ' + str(e))
        self.testLevel = 'NoTestRun'
        self.insidePackagePath = TypeOfFile.sfdxRootPath
        self.checkOnly = False 
        self.orgName = ''
        self.extraFlags = ' '
        self.waitTime = '999'
        self.pollingFrequency = 600 # For prod deployments, checks the progres each X seconds
        self.extraTests = ''
        self.testPackageList = ''

    def setTestLevel(self, testList, testLevel = None):
        if testLevel:
            self.testLevel = testLevel
            self.testList = testList
        elif testList != "":
            self.testList = testList 
            if self.extraTests:
                self.testList += ',' + self.extraTests 
            self.testLevel = 'RunSpecifiedTests'
        else:
            self.testLevel = 'RunLocalTests' if self.orgName == 'prod' else 'NoTestRun'

    def deploy(self, verbose=True):
        if os.path.exists(self.packageFolder):
            os.chdir(self.packageFolder)

        try:
            print('Deployer => Directory for deployment : ' + str(os.getcwd()))
        except Exception as e:
            print('Deployer => Error with cwd : ' + str(e))

        if self.orgName == 'prod':
            self.waitTime = '0'

        if self.testPackageList != '':
            deployCommand = 'sfdx force:source:deploy --apiversion 49.0 --json -u %s -w %s -p %s -l %s -r %s -c %s ' % (self.orgName, self.waitTime, self.insidePackagePath, self.testLevel, self.testPackageList, self.extraFlags)
            subprocess.run(deployCommand, capture_output=True, shell=True, timeout=36000)
            
        deployCommand = 'sfdx force:source:deploy --apiversion 49.0 --json -u %s -w %s -p %s -l %s %s' % (self.orgName, self.waitTime, self.insidePackagePath, self.testLevel, self.extraFlags)
        
        if hasattr(self, 'testList'):
            deployCommand += ' -r ' + str(self.testList)

        if self.checkOnly:
            deployCommand += ' -c '

        print('Deployer => SF deploy command: ' + deployCommand)

        if verbose:
            print('Deployer => Package Directory :')
            Configurable.list_files(self.packageFolder)

        print("Deployment in progress...")
        deployResultProcess = subprocess.run(deployCommand, capture_output=True, shell=True, timeout=36000)

        deployResult = deployResultProcess.stdout if deployResultProcess.stdout != b'' else deployResultProcess.stderr
        deployResult = deployResult.decode('utf8')

        try:
            success = deployResultProcess.returncode == 0
            parsed_result = json.loads(deployResult)
            if "result" in parsed_result and "deployedSource" in parsed_result["result"]:
                print("%15s   Name" % "Type")

                for item in parsed_result["result"]["deployedSource"]:
                    print("%15s   %s" % (item["type"], item["fullName"]))

        except Exception as e:
            print('Deployer => Error reading deployment result: %s' % str(e))
            if 'deployResult' in locals():
                print('Deployer => SF deployment result : ' + deployResult)
            raise OSError # TODO Write custom exception

        return [deployResult, success]
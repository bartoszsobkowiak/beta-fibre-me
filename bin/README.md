# Fibre.me Deployment script

Script written in python, to make deployment easier.
This client is a wrapper around git and sfdx command
which prepares a deployment package and tries to deploy 
it.

## Prerequisites

To work with this script you need to have installed:
 * python 3.x (tested on python 3.8)
 * sfdx command line client
 
## SFDX Authentication

Depends on how you want to use this script, it is recommended
to use either web based (requires user interaction) or jwt 
based authentication (suitable for CI). In case of JWT Flow,
you will need to generate openssl certificate and create 
a connected app. You can find guide how to authenticate 
with JWT with following link:

 * https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_auth_jwt_flow.htm

## Installation

To install required dependencies use below command:

    $ pip3 install -r requirements.txt

To verify that code works, please run:

    $ python3 deploy.py --help

## Usage

There are two modes, on how this script can be used:

 * branch
 * tag

### branch mode

The "branch" mode is designed to calculate difference between
two given git destinations. It is primarly designed to work
with branches, but it will also work when you specify tag 
or commit hash, as git handles that automatically.

Example usage:

```
$ python3 ./bin/deploy.py \
 --gitPath . -o fibre-me \
 -b feature/FM-1234-some-feature-branchw -s Dev -m branch \
 -r force-app/main/default/lwc/jsconfig.json
```

specific params are:

 * "-b" indicates branch we want to calculate diff for 
 * "-s" indicates the source git destination. 
   (in this example a branch Dev) which will be used
   as a point to start diff calculation
 * "-m" mode, in this case "branch" mode is selected
 * "--gitPath" - a path to root of the repository 
   ("." means current directory)
 * "-o" is an selected organization alias stored in sfdx
 * "-r" list of files (comma separated) to be ignored

### tag mode

The "tag" mode is designed to calculate difference between
given git branch and tag which start with specific prefix.
In this mode script will look for LAST tag (alphabetically)
with given prefix and will use it to calculate differences.

For example with list:
```
sit-202010271340
other-tag
sit-202010281258
some-other-tag
```

the tag `sit-202010281258` will be selected for deployment.

Example usage:

```
$ python3 ./bin/deploy.py \
 --gitPath . -o fibre-me \
 -b feature/FM-1234-some-feature-branchw -s sit -m tag \
 -r force-app/main/default/lwc/jsconfig.json
```
Comparing to previous examples, changes were made
to "-m" and "-s" params.
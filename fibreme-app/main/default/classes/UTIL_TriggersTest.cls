@isTest
private with sharing class UTIL_TriggersTest {

    private static final String ARBITRARY_TRIGGER = 'ArbitraryTriggerName';
    private static final String ARBITRARY_HANDLER = 'ArbitraryHandlerName';
    
    testmethod static void testTriggersEnableDisable() {
        // Disable
        UTIL_Triggers.disableTrigger('ACC_AllTriggers');
        System.assertEquals(FALSE, UTIL_Triggers.isTriggerEnabled('ACC_AllTriggers'));

        // Enable
        UTIL_Triggers.enableTrigger('ACC_AllTriggers');
        System.assertEquals(TRUE, UTIL_Triggers.isTriggerEnabled('ACC_AllTriggers'));

        // Expected behavior - enabled by default, disabled only when explicitly stated
        System.assertEquals(TRUE, UTIL_Triggers.isTriggerEnabled(ARBITRARY_TRIGGER));
        UTIL_Triggers.disableTrigger(ARBITRARY_TRIGGER);
        System.assertEquals(FALSE, UTIL_Triggers.isTriggerEnabled(ARBITRARY_TRIGGER));
    }

    testmethod static void testTriggersAllDisabledForUser() {
        update new User(Id = UserInfo.getUserId(), Disable_All_Triggers__c = TRUE);
        UTIL_Triggers.currentUser = null;
        System.assertEquals(FALSE, UTIL_Triggers.isTriggerEnabled('ACC_AllTriggers'));
    }

    testmethod static void testHandlersEnableDisable() {
        // Disable
        UTIL_Triggers.disableHandler('ACC50_DefaultTH');
        System.assertEquals(FALSE, UTIL_Triggers.isHandlerEnabled('ACC50_DefaultTH'));

        // Enable
        UTIL_Triggers.enableHandler('ACC50_DefaultTH');
        System.assertEquals(TRUE, UTIL_Triggers.isHandlerEnabled('ACC50_DefaultTH'));

        // Expected behavior - enabled by default, disabled only when explicitly stated
        System.assertEquals(TRUE, UTIL_Triggers.isHandlerEnabled(ARBITRARY_HANDLER));
        UTIL_Triggers.disableHandler(ARBITRARY_HANDLER);
        System.assertEquals(FALSE, UTIL_Triggers.isHandlerEnabled(ARBITRARY_HANDLER));
    }
}

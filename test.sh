#!/bin/sh
set -e

# community_config_path="scripts/8_communityConfig";
# community_template_id="04t3x000001Gv2WAAS";
# install_community_template=true;
# install_recipe=true;
# recipe_id="04t3x000001GuooAAC";
# while getopts "a:d:i:c:b:e:t:r:" o; do
#     case "${o}" in
#         a)
#             name=${OPTARG}
#             ;;
#         d)
#             duration=${OPTARG}
#             ;;
#         i)
#             import_path=${OPTARG}
#             ;;
#         c)
#             cpq_package=${OPTARG}
#             ;;
#         b)
#             b2b_package=${OPTARG}
#             ;;
#         e)
#             recipe_id=${OPTARG}
#             ;;
#         t)
#             community_template_id=${OPTARG}
#             ;;
#         r)
#             isPreview=${OPTARG}
#             ;;            
#         *)
#             ;;
#     esac
# done

# shift $((OPTIND-1))

# if [ $install_recipe = true ]; then
#   echo 'Installing recipe: Customer Sales Process Flows'
#   sfdx force:package:install -u $name --noprompt --package $recipe_id --wait 30
# fi

# if [ $install_community_template = true ]; then
#   echo 'Installing community template'
#   sfdx force:package:install -u $name --noprompt --package $community_template_id --wait 30
#   echo 'Creating community...'
#   sfdx force:community:create -u $name --name 'Customer Portal' --templatename 'Enxoo Customer Portal' --urlpathprefix customers
#   echo 'Community created, wait 5min...'
#   sleep 300
#   sfdx force:mdapi:deploy -u $name --ignoreerrors --deploydir $community_config_path --wait 30
#   sfdx force:community:publish -u $name --name 'Customer Portal'
#   echo 'Community deployed!'
# fi

# sfdx force:data:bulk:upsert -s enxB2B__POP__c -f scripts/X_extendedData/POPs.csv -i ENXB2B__TECH_EXTERNAL_ID__c -w 5
# sfdx force:data:bulk:upsert -s enxCPQ__Network_Node__c -f scripts/X_extendedData/Nodes.csv -i ENXCPQ__TECH_EXTERNAL_ID__c -w 5

SERVER_KEY="-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEAxmNOWzGtZY5M3K4VrxRShyLsy1lLZlgoUi8fwlS5g9aXbyeg
fNb6f/Dc1iZKZhx7pUVnHE9RbNXLVxjoQA1OXf8zTRruuESymNDEc3Zv0fv8LU6E
UN+g127YvlpQWkNFwzymzuw+rKDA8+umNsDH+pP3xnByj7MEaHPN5q/6DyxIyLsN
A2h+H/Hg55OOv5HQr6j2UCLITN+vzxjBE/JacAkNwj6xRj9fuINrMbsSv++HpAYr
jmSImsDmVoZkzr1s30D7RcOVbA5TR3hcSObCUozVoi126GhucxB5HjgaReHG5kf3
3mrexY29SxfxmrbRwHx/vmPCaNY8/BDmZeHJ6QIDAQABAoIBAFQtjvHjAlA1MSfy
NgEz/lCY4SDnoraqcXFtzWwHxy/JvjfL+K0ACwp+iTXUyXzKWSynZjj87u/fR4iN
aP7Eppv7zSjKZMCLZbyx2Kmld40B/t66kio+uWCwCAe9CptCMDfHZIAofe+Bxl25
f617317Tqi7pR7Lxos0vVg6sXM5SnHUUlJVMZuwUgMfV1HBZ3elyZAqCDS1hBz+8
HPkFY48Nep7lyoilwoR1fmu0Srj1oEMSEsJMNwjnapiB7G5FXKTDARyOIgs+ozQd
UOvEvqHl8OKRHM3gGgUkm0L8c5kkxlpZQq9h+bwhqJCQMEOWdeT77JCmurpjyiqM
POS9cgECgYEA74VUUWvemomnSCMmLRNTBR51ob7yKB4GPOGMCuvvHnbYE3zfzuIz
/ys8ZhYlFhZpREeq/fRDjc/8N7wMJKQ5Bm12j0VjK1u9LMpoXz9Jxc4Ums4oOeCo
pL5fdL41tCUVT8rcX6D8+GctOeiTQCEtQXsk3sMm0Aus/8RrVg3BEHUCgYEA1AmB
KDSL/GbnfuB/+aAJDFjIkGXUM1YxovB28Sy0ULOCyPXTwBXT5CPjIH5ld63wrE11
MiDEPqX1Z5uKaGBDfVRVtadFB7ssfO0An5GD2bHLbSABtYcKip1ub4lUFmO9vYXF
eO8GahGH8fvFylU9nnQEr+PJqB3War8/RagDpSUCgYEA1LS65HO/A84q2E0eofj/
SRR7smA3/JhtQbOrAifz+kHgMa47syX+tyRWRvJzjOHqKdDbcRtywoVpnrVIs2GD
osmi+CjOoLqfjR9TAQLbKOaSNcfOwVdZbLQDYVFpr8ke2/VzLxlwjchSyZJY8iKE
ZhpN6+Wymh9ESv7Ls2gTg+ECgYEArKFUW1R0yDlLrlu/NZAa7cd2+0E8g9NgNCWM
4Qwhfja4ez7ReGkNC4hASds+ZzLhaJbjnL1aqRra8tCK3jHJoMdBxNR7ejDkpo0a
3c9ORrXfRmgxnAZnCbXlDMAFCvmHqD66KJyKX18IjHYMcY/2zAPvMPjG3K32PRCZ
xe9lkFUCgYEAhozMxh/+mT+Weqi8E8V5OGSevl1vzcQqSITm32ZQr09wVIHXLL0A
bjK8poyzTqbtmJ5OrKxoF0snFAUCb2ozr0nqpolkptnw/mxfR8O4ZtCVpvfBdMbY
UOxGYeD5xv/VQME2UQ0D3AG98s83TR49Opitn4p4/OqlecGdE6caYNo=
-----END RSA PRIVATE KEY-----";

# SERVER_KEY="-----BEGIN RSA PRIVATE KEY-----MIIEpQIBAAKCAQEAxmNOWzGtZY5M3K4VrxRShyLsy1lLZlgoUi8fwlS5g9aXbyegfNb6f/Dc1iZKZhx7pUVnHE9RbNXLVxjoQA1OXf8zTRruuESymNDEc3Zv0fv8LU6EUN+g127YvlpQWkNFwzymzuw+rKDA8+umNsDH+pP3xnByj7MEaHPN5q/6DyxIyLsNA2h+H/Hg55OOv5HQr6j2UCLITN+vzxjBE/JacAkNwj6xRj9fuINrMbsSv++HpAYrjmSImsDmVoZkzr1s30D7RcOVbA5TR3hcSObCUozVoi126GhucxB5HjgaReHG5kf33mrexY29SxfxmrbRwHx/vmPCaNY8/BDmZeHJ6QIDAQABAoIBAFQtjvHjAlA1MSfyNgEz/lCY4SDnoraqcXFtzWwHxy/JvjfL+K0ACwp+iTXUyXzKWSynZjj87u/fR4iNaP7Eppv7zSjKZMCLZbyx2Kmld40B/t66kio+uWCwCAe9CptCMDfHZIAofe+Bxl25f617317Tqi7pR7Lxos0vVg6sXM5SnHUUlJVMZuwUgMfV1HBZ3elyZAqCDS1hBz+8HPkFY48Nep7lyoilwoR1fmu0Srj1oEMSEsJMNwjnapiB7G5FXKTDARyOIgs+ozQdUOvEvqHl8OKRHM3gGgUkm0L8c5kkxlpZQq9h+bwhqJCQMEOWdeT77JCmurpjyiqMPOS9cgECgYEA74VUUWvemomnSCMmLRNTBR51ob7yKB4GPOGMCuvvHnbYE3zfzuIz/ys8ZhYlFhZpREeq/fRDjc/8N7wMJKQ5Bm12j0VjK1u9LMpoXz9Jxc4Ums4oOeCopL5fdL41tCUVT8rcX6D8+GctOeiTQCEtQXsk3sMm0Aus/8RrVg3BEHUCgYEA1AmBKDSL/GbnfuB/+aAJDFjIkGXUM1YxovB28Sy0ULOCyPXTwBXT5CPjIH5ld63wrE11MiDEPqX1Z5uKaGBDfVRVtadFB7ssfO0An5GD2bHLbSABtYcKip1ub4lUFmO9vYXFeO8GahGH8fvFylU9nnQEr+PJqB3War8/RagDpSUCgYEA1LS65HO/A84q2E0eofj/SRR7smA3/JhtQbOrAifz+kHgMa47syX+tyRWRvJzjOHqKdDbcRtywoVpnrVIs2GDosmi+CjOoLqfjR9TAQLbKOaSNcfOwVdZbLQDYVFpr8ke2/VzLxlwjchSyZJY8iKEZhpN6+Wymh9ESv7Ls2gTg+ECgYEArKFUW1R0yDlLrlu/NZAa7cd2+0E8g9NgNCWM4Qwhfja4ez7ReGkNC4hASds+ZzLhaJbjnL1aqRra8tCK3jHJoMdBxNR7ejDkpo0a3c9ORrXfRmgxnAZnCbXlDMAFCvmHqD66KJyKX18IjHYMcY/2zAPvMPjG3K32PRCZxe9lkFUCgYEAhozMxh/+mT+Weqi8E8V5OGSevl1vzcQqSITm32ZQr09wVIHXLL0AbjK8poyzTqbtmJ5OrKxoF0snFAUCb2ozr0nqpolkptnw/mxfR8O4ZtCVpvfBdMbYUOxGYeD5xv/VQME2UQ0D3AG98s83TR49Opitn4p4/OqlecGdE6caYNo=-----END RSA PRIVATE KEY-----";

SERVER_KEY.replace(/\\n/gm, '\n');

umask  077 ; echo $SERVER_KEY | base64 -d -i > keys/server.key

https://github.com/auth0/node-jsonwebtoken/issues/642

importer tool:
https://enxooteam.atlassian.net/wiki/spaces/ECPQKB/pages/1645707272/Importer+Tool+-+Migrating+Product+Catalogue?focusedCommentId=3005776010#comment-3005776010